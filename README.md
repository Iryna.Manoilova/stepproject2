Состав участников проекта:  
Ирина Манойлова  
Виктория Колот

Список использованных технологий:
    "browser-sync",
    "gulp",
    "gulp-autoprefixer",
    "gulp-clean",
    "gulp-clean-css",
    "gulp-concat",
    "gulp-file-include",
    "gulp-imagemin",
    "gulp-js-minify",
    "gulp-postcss",
    "gulp-rename",
    "gulp-sass",
    "gulp-uglify",
    "postcss",
    "sass"

Какие задачи выполнял кто из участников:
Виктория Колот - выполняла задание для студента №2,  
Ирина Манойлова - выполняла задание для студента №1, сборку gulp