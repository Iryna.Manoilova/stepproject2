const gulp = require('gulp'),
	concat = require('gulp-concat'),
	clean = require('gulp-clean'),
	browserSync = require('browser-sync').create(),
	sass = require('gulp-sass')(require('sass')),
	//uglify = require('gulp-uglify');
	imagemin = require('gulp-imagemin'),
	autoprefixer = require('autoprefixer'),
	postcss = require('gulp-postcss'),
	cleanCSS = require('gulp-clean-css'),
	rename = require("gulp-rename"),
	// minifyjs = require('gulp-js-minify'),
	uglify = require('gulp-uglify'),
	fileinclude = require('gulp-file-include');
	

/*** PATHS ***/
const paths = {
	src: {
		html:'./src/html/index.html',
		scss: './src/scss/**/*.scss',
		js: './src/js/*.js',
		img: './src/img/**/*.{jpg,png,svg}'
	},
	dist: {
		html:'./',
		css: './dist/css/',
		js: './dist/js/',
		img:'./dist/img/',
		self: './dist/',
	}
}

/*** FUNCTIONS ***/

const distJS = () =>
	gulp
		.src(paths.src.js)
		.pipe(concat('script.js'))
		.pipe(gulp.dest(paths.dist.js))
		// .pipe(minifyjs())
		.pipe(uglify())
		.pipe(rename({
            extname: ".min.js"
        }))
		.pipe(gulp.dest(paths.dist.js))
		.pipe(browserSync.stream())

const distCSS = () =>
	gulp
		.src(paths.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss([autoprefixer({
            overrideBrowserslist: ["last 5 versions"],
            cascade: true,
        })]))
		.pipe(gulp.dest(paths.dist.css))
		.pipe(cleanCSS())
		.pipe(rename({
            extname: ".min.css"
        }))
		.pipe(gulp.dest(paths.dist.css))
		.pipe(browserSync.stream())


const distIMG = () =>
	gulp
		.src(paths.src.img)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.dist.img))
		.pipe(browserSync.stream())

const distHTML = () =>
	gulp
		.src(paths.src.html)
		.pipe(fileinclude())
		.pipe(gulp.dest(paths.dist.html))
		.pipe(browserSync.stream())

const cleandist = () => gulp.src(paths.dist.self, { allowEmpty: true }).pipe(clean())

const watcher = () => {
	browserSync.init({
		server: {
			baseDir: './',
		},
	})

	gulp.watch(paths.src.scss, distCSS).on('change', browserSync.reload)
	gulp.watch(paths.src.js, distJS).on('change', browserSync.reload)
	gulp.watch(paths.src.img, distIMG).on('change', browserSync.reload)
	gulp.watch(paths.src.html, distHTML).on('change', browserSync.reload)
}

/*** TASKS ***/
gulp.task('clean', cleandist)
gulp.task('distCSS', distCSS)
gulp.task('distJS', distJS)
gulp.task('distIMG', distIMG)
gulp.task('distHTML', distHTML)


gulp.task('default', gulp.series(cleandist,distHTML, distCSS, distJS, distIMG, watcher))


